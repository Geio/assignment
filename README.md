This is as much as I could muster up from the given assignment in about 4 hours of trying to create my very first Web App using ASP.NET. Turns out that I still have a lot to learn here and would have needed a lot more time to create something that, well.... actually works :) 

## Functionality that works: 
- Creating a user (was already premade during the generation of the project, so can't really take credit for this, can I?) 
- Viewing the list of rentable equipment as a logged in user

## Functionality that is missing:
- Everything else :(


## To run the project and navigate in it (in whatever exists)
1. Clone the repository 
2. Open the solution 
3. Run ```dotnet ef database update``` command in console to run the migration to create user databases 
4. Run ```dotnet watch``` command to view the solution 
5. Register an account and confirm the email by clicking on the provided link on the page. After that you can log in with the registered user credentials.
6. Click on "Rentals" tab to see the available rental equipment 
7. And voilà, that ends the tour of the whole Web Application :)
