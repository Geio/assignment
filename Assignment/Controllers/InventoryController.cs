﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using Assignment.Models;

namespace Assignment.Controllers
{
    public class InventoryController : Controller
    {
        static IList<Inventory> inventoryList = new List<Inventory>{
            new Inventory() { Id = 1, Name = "Caterpillar bulldozer", Type = "Heavy" } ,
            new Inventory() { Id = 2, Name = "KamAZ truck", Type = "Regular" } ,
            new Inventory() { Id = 3, Name = "Komatsu crane", Type = "Heavy" } ,
            new Inventory() { Id = 4, Name = "Volvo steamroller", Type = "Regular" } ,
            new Inventory() { Id = 5, Name = "Bosch jackhammer", Type = "Specialized" } ,

        };

        // GET: InventoryController
        public ActionResult Index()
        {
            return View(inventoryList);
        }

        public void ProcessRental()
        {
            NameValueCollection values = Request.Form;
        }

        // GET: InventoryController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: InventoryController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: InventoryController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: InventoryController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: InventoryController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: InventoryController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: InventoryController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
