﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment.Models
{
    [Table("Inventory")]
    public class Inventory
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Type { get; set; }
    }
}
